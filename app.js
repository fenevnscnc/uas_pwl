var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var flash = require('express-flash');
var session = require('express-session');

var indexRouter       = require('./routes/index');
var perlombaanRouter  = require('./routes/Perlombaan');
var pesertaRouter     = require('./routes/Peserta');
var usersRouter       = require('./routes/Users');
var profileRouter     = require('./routes/Profile');

// Settup DB connection
var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/crud_pegawai', { useNewUrlParser: true })
 .then(() => console.log('berhasil terhubung'))
 .catch((err) => console.error(err));
console.log('mongodb terkoneksi');
// End Settup DB

var app = express();

// View engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({secret:'brotherhood2013'}));
app.use(flash());

// Routers included
app.use('/', indexRouter);
app.use('/peserta', pesertaRouter);
app.use('/perlombaan', perlombaanRouter);
app.use('/users', usersRouter);
app.use('/profile', profileRouter);


// Catch 404 and forward to error handler
app.use(function(req, res, next) {
  res.locals.query = req.query;
  res.locals.url   = req.originalUrl;
  next(createError(404));
});


// Error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // Render the error page
  res.status(err.status || 500);
  res.render('error');
});


module.exports = app;
