var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var pesertaSchema = new Schema(
 {
  nama: { type: String, required: true },
  email: { type: String, required: true },
  umur: { type: String, required: true },
  id_perlombaan: { type: Schema.Types.ObjectId, ref: 'perlombaan', required: true }
 }
);


// const perserta = mongoose.model('peserta', pesertaSchema);
// module.exports = perserta;

module.exports = mongoose.model('peserta', pesertaSchema);
