var mongoose = require('mongoose');
const Perlombaan = require('../models/Perlombaan');
var Peserta = require('../models/Peserta');

var pesertaController = {};

// Create data
pesertaController.save = function(req, res){
    // var peserta = new Peserta(req.body);
 
    // peserta.save(function(err){
    //     if(err){
    //         console.log(err);
    //         res.render('index');
    //     }else{
    //         console.log('save sukses');
    //         res.redirect('../peserta/list');
    //     }
    // });

    console.log(req.body.nama);
    Peserta.find({nama : req.body.nama}, function (err, docs) {
        if (docs.length){
            console.log('data sudah ada');
            res.redirect('../peserta/list');
        }else{
            var peserta = new Peserta(req.body);
    
            peserta.save(function(err){
                if(err){
                    console.log(err);
                    res.redirect('../peserta/list');
                }else{
                    console.log('save sukses');
                    res.redirect('../peserta/list');
                }
            });
        }
    });

};

// Read data by id peserta
pesertaController.edit = function(req, res){
    var id = req.params._id;
    Peserta.findOne({ _id: id }, function(err, peserta){
        if(peserta){
            console.log(peserta);
            Perlombaan.find({}, function(err, perl){
                Perlombaan.findOne({ _id:peserta.id_perlombaan }, function(err, x){
                    console.log(x);
                    res.render('editpeserta', {peserta: peserta, title: 'CRUD', perlombaan: perl, x: x});
                });
            });
        }else{
            res.redirect('../');
        }
    });
};

// Update data
pesertaController.update = function(req, res){
    Peserta.findByIdAndUpdate(req.params._id, {
        $set: { 
            nama: req.body.nama,
            email: req.body.email,
            umur: req.body.umur,
            id_perlombaan: req.body.id_perlombaan
        }
    }, { new: true }, function(err, peserta){
        if(err){
            console.log(err);
            res.render('editpeserta', { peserta: req.body });
        }
        res.redirect('http://localhost:3000');
    });
};

// Delete data
pesertaController.delete = function(req, res){
    Peserta.findOne({ _id: req.params._id }, function(err, row){
        if(row){
            console.log(row);
            Peserta.remove({ _id: req.params._id }, function(err){
                if(err){
                    console.log('delete error', err); 
                }else{
                    console.log('delete sukses');
                    res.redirect('http://localhost:3000');
                }
            }); 
        }else{
            res.redirect('http://localhost:3000');  
        }
    });
};

module.exports = pesertaController;