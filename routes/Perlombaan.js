var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var perController = require('../controllers/PerlombaanController');
var perlombaan = require('../models/Perlombaan');
var Auth_mdw = require('../middlewares/auth');
var session_store;

router.get('/', Auth_mdw.check_login, function(req, res, next){
    session_store = req.session;
    perlombaan.find({}, function(err, perl){
        console.log(perl);
        res.render('perlombaan', { perlombaan: perl, title: 'Perlombaan', sessions: session_store});
    }).select('_id nama');
});

// router.get('/list', Auth_mdw.check_login, function(req, res, next){
//     perlombaan.find({}, function(err, perl){
//         console.log(perl);
//         res.render('perlombaan', { perlombaan: perl, title: 'Perlombaan' });
//     }).select('_id nama');
// });

router.post('/tambah', Auth_mdw.check_login, function(req, res){
    perController.save(req, res);
    console.log('mau di save nih');
});

// Delete
router.get('/delete/:_id', Auth_mdw.check_login, function(req,res){
    perlombaan.delete(req, res)
});



module.exports = router;
