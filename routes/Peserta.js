var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var peserta = require('../controllers/PesertaController');
var model_Peserta = require('../models/Peserta');
var perlombaan = require('../models/Perlombaan');
var Auth_mdw = require('../middlewares/auth');
var session_store;
var moment = require('moment');


// View Create
router.get('/add', Auth_mdw.check_login, function(req, res){
    res.locals.res = res
    perlombaan.find({}, function(err, perl){
        console.log(perl);
        res.render('peserta', { perlombaan: perl, title: 'Input Data Peserta Lomba', sessions: session_store});
    }).select('_id nama');
});


// Read All
router.get('/list', Auth_mdw.check_login, function(req, res, next) {
    session_store = req.session;
    model_Peserta.find({}, function(err, data_peserta){
        console.log(data_peserta);
        res.render('list_peserta', { peserta: data_peserta, title: 'List Peserta Lomba', sessions: session_store});
    }).populate('id_perlombaan').select('_id nama email umur');
});


// Post Create
router.post('/tambah', Auth_mdw.check_login, function(req, res){
    peserta.save(req, res);
});


// Get Read by id
router.get('/edit/:_id', Auth_mdw.check_login, function(req, res){
    peserta.edit(req, res);
});


// Post Update
router.post('/update/:_id', Auth_mdw.check_login, function(req, res){
    peserta.update(req, res);
});


// Delete
router.get('/delete/:_id', Auth_mdw.check_login, function(req,res){
    peserta.delete(req, res)
});


module.exports = router;
